import React, { useState, useEffect } from "react";
import Header from "./composition/Header/Header";
import "./styles/reset.scss";
import "./styles/App.scss";
import Footer from "./composition/Footer/Footer";
import Cars from "./pages/Cars/Cars";
import Favorites from "./pages/Favorites/Favorites";
import Basket from "./pages/Basket/Basket";
import NotFound from "./pages/NotPage/NotFound";
import { BrowserRouter, Routes, Route } from "react-router-dom";


function App() {
  const [favorites, setFavorites] = useState([]);
  const [basket, setBasket] = useState([]);

  const removeFromFavorites = (itemId) => {
    const newFavorites = favorites.filter((favorite) => favorite.id !== itemId);
    setFavorites(newFavorites);
  };

  const removeFromBasket = (itemId) => {
    const newBasket = basket.filter((item) => item.id !== itemId);
    setBasket(newBasket);
  };
  
    useEffect(() => {
      localStorage.setItem("favorites", JSON.stringify(favorites));
    }, [favorites]);
  
    useEffect(() => {
      localStorage.setItem("basket", JSON.stringify(basket));
    }, [basket]);


  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route
            index
            element={<Cars favorites={favorites} setFavorites={setFavorites} basket={basket} setBasket={setBasket} />}
          />
          <Route path="/cars" element={<Cars favorites={favorites} setFavorites={setFavorites} basket={basket} setBasket={setBasket} />} />
          <Route path="/favorites" element={<Favorites favorites={favorites}  removeFromFavorites={removeFromFavorites}/>} />
          <Route path="/basket" element={<Basket basket={basket} removeFromBasket={removeFromBasket} />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
        <Footer />
      </BrowserRouter>
    </div>
  );
}

export default App;

