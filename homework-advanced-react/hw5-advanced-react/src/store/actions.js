import {createAction} from "@reduxjs/toolkit";

import {sendRequest} from '../helpers/sendRequst.js'

export const actionAddToFavorite = createAction("ACTION_ADD_TO_FAVORITE")
export const actionAddToBasket = createAction("ACTION_ADD_CINEMA_ARR")
export const actionAddProduct = createAction("ACTION_ADD_CINEMA_ITEM")

export const actionProduct = (page) => (dispatch) => {

	console.log('actionFetchCinemaSlider - page ',page);
	return sendRequest(`../../public/products.json`)
		.then(({results}) => {
			dispatch(actionAddToCinemaArr(results))
		})
}
export const actionAdd = (id) => (dispatch) => {
	return sendRequest(`../data/products.json`)
		.then((data) => {
			dispatch(actionAddToCinemaItem(data))
		})
}
