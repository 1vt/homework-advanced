import React, { useState } from "react";
import "./styles/App.css";
import MyButton from "./components/button/MyButton";
import MyModal from "./components/modal/MyModal";
import { useEffect } from "react";
import { useRef } from "react";
import ModalHeader from "./components/modal/ModalHeader";
import ModalBody from "./components/modal/ModalBody";
import ModalWrapper from "./components/modal/ModalWrapper";
import ModalFooter from "./components/modal/ModalFooter";
import ModalFooters from "./components/modal/ModalFooters";

function App() {
  const [isOpen, setIsOpen] = useState(false);
  const [isOpen2, setIsOpen2] = useState(false);

  let menuRef = useRef();

  useEffect(() => {
    let handler = (e) => {
      if (e.target) {
        setIsOpen(false);
      }
    };
    document.addEventListener("mousedown", handler);
  });

  useEffect(() => {
    let handler = (e) => {
      if (e.target) {
        setIsOpen2(false);
      }
    };
    document.addEventListener("mousedown", handler);
  });

  return (
    <div className="App">
      <MyButton onClick={() => setIsOpen(true)}>Open first modal</MyButton>
      <MyButton onClick={() => setIsOpen2(true)}>Open second modal</MyButton>

      <MyModal open={isOpen} onClose={() => setIsOpen(false)} ref={menuRef}>
        <ModalWrapper></ModalWrapper>
        <ModalHeader>Product Delete!</ModalHeader>
        <ModalBody>
          By clicking the "Yes, Delete" button, PRODUCT NAME will be delete
        </ModalBody>
        <ModalFooter></ModalFooter>
      </MyModal>

      <MyModal open={isOpen2} onClose={() => setIsOpen2(false)} ref={menuRef}>
        <ModalHeader>Add Product "NAME"</ModalHeader>
        <ModalBody>Description for your product</ModalBody>
        <ModalFooters></ModalFooters>
      </MyModal>
    </div>
  );
}

export default App;
