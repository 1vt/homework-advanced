import React from "react";
import classes from "./ModalFooter.module.css";

export default function ModalFooter() {
  return (
    <div className={classes.firstModalBtn}>
      <button className={classes.cancelBtn}>NO, CANCEL</button>
      <button className={classes.yesBtn}>YES, DELETE</button>
    </div>


  );
}
// NO, CANCEL
// YES, DELETE