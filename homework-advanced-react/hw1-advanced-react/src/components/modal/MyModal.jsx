import React from 'react'
import classes from './MyModal.module.css'

export default function MyModal({open, children, onClose}) {
    if (!open) return null

  return (
    <>
        <div className={classes.overlay}></div>
        <div className={classes.myMdl}>
        <button onClick={onClose} className={classes.closeX}>X</button>
        {children}
        </div>
    </>
  )
}
