import {createReducer} from "@reduxjs/toolkit";
import * as actions from "./actions.js";
const initialState = {
	favorites: [],
	basket: [],
}
export default createReducer(initialState, {
	[actions.handleAddToCart]: (state, {payload}) => {
		state.favorites = [ ...state.favorites, payload]
	},
	[actions.handleAddToCart]: (state, {payload}) => {
		state.handleAddToCart = [ ...state.cinemaArr, payload]
	},
	[actions.handleAddToBasket]: (state, {payload}) => {
		state.handleAddToBasket = payload
	}
})