import React from 'react';
import Header from '../../composition/Header/Header';
import "./Basket.scss"

const Basket = ({ basket, removeFromBasket }) => {
    const handleRemoveFromBasket = (item) => {
        removeFromBasket(item);
    }
    return (
        <>
            <Header />
            <div className='basket'>
                <h2>Basket</h2>
                {basket && basket.length > 0 ? (
                    <ul>
                        {basket.map((item) => (
                            <li key={item.id}>
                                <img src={item.imagePath} alt={item.name} className='img'/>
                                {item.name} - {item.price}
                                <button onClick={() => removeFromBasket(item.id)}>Remove from Basket</button>
                            </li>
                        ))}
                    </ul>
                ) : (
                    <p>Your basket is empty.</p>
                )}
            </div>
        </>
    );
};

export default Basket;

