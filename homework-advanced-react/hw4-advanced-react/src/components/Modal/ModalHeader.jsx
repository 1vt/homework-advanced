import React from 'react'
import classes from './ModalHeader.module.css' 

export default function ModalHeader({children, ...props}) {
  return (
        <h2 {...props} className={classes.h1}>{children}</h2>
  )
}
