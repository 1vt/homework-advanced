import React from "react";
import classes from "./ModalFooters.module.css";

export default function ModalFooters() {
  return (
    <div className={classes.firstModalBtns}>
      <button className={classes.addFavor}>ADD TO FAVORITE</button>
    </div>
  );
}