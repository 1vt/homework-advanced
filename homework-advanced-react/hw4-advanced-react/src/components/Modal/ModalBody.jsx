import React from 'react'
import classes from './ModalBody.module.css'

export default function ModalBody({children, ...props}) {
  return (
    <div><p  {...props} className={classes.bodyP}>{children}</p></div>
  )
}
