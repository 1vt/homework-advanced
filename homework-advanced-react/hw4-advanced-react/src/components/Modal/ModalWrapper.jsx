import React from 'react'
import classes from './ModalWrapper.module.css'

export default function ModalWrapper() {
  return (
    <div className={classes.wrapperDiv}><p className={classes.wrapperP}></p></div>
  )
}
