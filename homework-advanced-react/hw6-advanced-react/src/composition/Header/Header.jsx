import logoicon from "./icons/carlogo.png";
import "./Header.scss";
import React, { useEffect } from "react";
import favHeart from "./icons/favotire.jpg";
import basket from "./icons/basket.png";
import MySVGCar from "../../components/MySVGComponent/MySVGCar";
import { Link } from "react-router-dom";

const Header = ({ addedCount, addedBasket }) => {
  useEffect(() => {
    localStorage.setItem("addedCount", addedCount);
    localStorage.setItem("addedBasket", addedBasket);
  }, [addedCount, addedBasket]);

  return (
    <header className="header">
      <li>
        <Link to="/">
          <img src={logoicon} alt="logo" className="logo" />
        </Link>
      </li>
      <nav className="nav">
        <li>
          <Link to="/" className="nav-text">
            CARS
          </Link>
        </li>

        <li>
          <Link to="#" className="nav-text">
            TYPES
          </Link>
        </li>

        <li>
          <Link to="#" className="nav-text">
            BRANDS
          </Link>
        </li>

        <li>
          <Link to="/favorites" className="nav-text">
            Favorites
          </Link>
        </li>
        <li>
          <Link to="/basket" className="nav-text">
            Basket
          </Link>
        </li>
        <aside className="headerAside">
          <div>
            <p className="favCount">{addedCount}</p>
            <img src={favHeart} alt="heart" className="fav" />
          </div>
          <form>
            <input placeholder="search" className="nav-input"></input>
          </form>
          <div>
            <p className="basCount">{addedBasket}</p>
            <img src={basket} alt="basket" className="basket" />
          </div>
        </aside>
      </nav>
    </header>
  );
};
export default Header;
