import "./Footer.scss"
import React from 'react'

export default function Footer() {
  return (
    <footer className="footer">
        <td className="footer-td">
            <th>Need Help</th>
            <th>Company</th>
            <th>More Info</th>
            <th>Location</th>
            <th>About Us</th>
        </td>
        <p>All Rights Reserved 2023</p>
    </footer>
  )
}
