// Basket.jsx
import React, { useState } from 'react';
import Header from '../../composition/Header/Header';
import "./Basket.scss";
import { useFormik } from 'formik';
import * as Yup from 'yup';

const Basket = ({ basket, removeFromBasket }) => {
    const [showForm, setShowForm] = useState(false);
    const [showSuccessPopup, setShowSuccessPopup] = useState(false);

    const formik = useFormik({
        initialValues: {
            firstName: "",
            lastName: "",
            age: "",
            address: "",
            mobile: ""
        },
        validationSchema: Yup.object({
            firstName: Yup.string().required('Required'),
            lastName: Yup.string().required('Required'),
            age: Yup.number().required('Required').positive('Must be a positive number'),
            address: Yup.string().required('Required'),
            mobile: Yup.string().required('Required'),
        }),
        onSubmit: (values) => {
            console.log("Checkout", values, basket);
            setShowForm(false); 
            setShowSuccessPopup(true); // Показ pop-up про успішну покупку
        },
    });

    const handleRemoveFromBasket = (item) => {
        removeFromBasket(item);
    }

    const handleSuccessPopupClose = () => {
        setShowSuccessPopup(false);
    }

    return (
        <>
            <Header />
            <div className='basket'>
                <h2>Basket</h2>
                {basket && basket.length > 0 ? (
                    <>
                        <ul>
                            {basket.map((item) => (
                                <li key={item.id}>
                                    <img src={item.imagePath} alt={item.name} className='img'/>
                                    {item.name} - {item.price}
                                    <button onClick={() => removeFromBasket(item.id)}>Remove from Basket</button>
                                </li>
                            ))}
                        </ul>
                        <button onClick={() => setShowForm(true)} className='btn_buy'>BUY IT NOW</button>
                        {showForm && (
                            <div className="overlay">
                                <div className="form-popup">
                                    <form onSubmit={formik.handleSubmit}>
                                        <label>
                                            First Name:
                                            <input type="text" name="firstName" value={formik.values.firstName} onChange={formik.handleChange} onBlur={formik.handleBlur} required />
                                            {formik.touched.firstName && formik.errors.firstName ? <div>{formik.errors.firstName}</div> : null}
                                        </label>
                                        <label>
                                            Last Name:
                                            <input type="text" name="lastName" value={formik.values.lastName} onChange={formik.handleChange} onBlur={formik.handleBlur} required />
                                            {formik.touched.lastName && formik.errors.lastName ? <div>{formik.errors.lastName}</div> : null}
                                        </label>
                                        <label>
                                            Age:
                                            <input type="number" name="age" value={formik.values.age} onChange={formik.handleChange} onBlur={formik.handleBlur} required />
                                            {formik.touched.age && formik.errors.age ? <div>{formik.errors.age}</div> : null}
                                        </label>
                                        <label>
                                            Address:
                                            <input type="text" name="address" value={formik.values.address} onChange={formik.handleChange} onBlur={formik.handleBlur} required />
                                            {formik.touched.address && formik.errors.address ? <div>{formik.errors.address}</div> : null}
                                        </label>
                                        <label>
                                            Mobile:
                                            <input type="tel" name="mobile" value={formik.values.mobile} onChange={formik.handleChange} onBlur={formik.handleBlur} required />
                                            {formik.touched.mobile && formik.errors.mobile ? <div>{formik.errors.mobile}</div> : null}
                                        </label>
                                        <button type="submit" disabled={formik.isSubmitting}>Checkout</button>
                                        <button type="button" onClick={() => setShowForm(false)}>Close</button>
                                    </form>
                                </div>
                            </div>
                        )}
                        {showSuccessPopup && (
                            <div className="overlay">
                                <div className="success-popup">
                                    <p className='p_success'>Thank you for your purchase!</p>
                                    <button onClick={handleSuccessPopupClose}>Close</button>
                                </div>
                            </div>
                        )}
                    </>
                ) : (
                    <p>Your basket is empty.</p>
                )}
            </div>
        </>
    );
};

export default Basket;
