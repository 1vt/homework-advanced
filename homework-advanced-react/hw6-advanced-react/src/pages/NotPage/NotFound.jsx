import Header from "../../composition/Header/Header";
import React from "react";
import NotPage from "./img/notfound.png";
import "./NotFound.scss"

const NotFound = () => {
  return (
    <>
      <Header></Header>
      <div className="notpage">
        <img src={NotPage}></img>
        <p>Sorry</p>
        <br />
        <p>This page did not find</p>
      </div>
    </>
  );
};

export default NotFound;
