// Cars.test.js
import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Cars from './Cars';

test('renders Cars component', () => {
  render(<Cars favorites={[]} setFavorites={() => {}} basket={[]} setBasket={() => {}} />);
  const switchViewButton = screen.getByText(/Switch to Table View/i);
  expect(switchViewButton).toBeInTheDocument();
});

test('toggles view mode when switch button is clicked', () => {
  render(<Cars favorites={[]} setFavorites={() => {}} basket={[]} setBasket={() => {}} />);
  const switchViewButton = screen.getByText(/Switch to Table View/i);
  fireEvent.click(switchViewButton);
  expect(screen.getByText(/Switch to Card View/i)).toBeInTheDocument();
});
