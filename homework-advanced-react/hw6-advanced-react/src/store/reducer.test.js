import reducer, { initialState } from './reducer';

test('reducer handles ADD_TO_CART action', () => {
  const action = { type: 'ADD_TO_CART', payload: { id: 1, name: 'Car 1' } };
  const newState = reducer(initialState, action);
  expect(newState.basket).toEqual([{ id: 1, name: 'Car 1' }]);
  expect(newState.addedBasket).toBe(1);
});
