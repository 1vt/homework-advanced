// DisplayModeContext.js
import React, { createContext, useState, useContext } from 'react';

const DisplayModeContext = createContext();

const DisplayModeProvider = ({ children }) => {
  const [displayMode, setDisplayMode] = useState('cards');

  const toggleDisplayMode = () => {
    setDisplayMode((prevMode) => (prevMode === 'cards' ? 'table' : 'cards'));
  };

  return (
    <DisplayModeContext.Provider value={{ displayMode, toggleDisplayMode }}>
      {children}
    </DisplayModeContext.Provider>
  );
};

const useDisplayMode = () => {
  const context = useContext(DisplayModeContext);
  if (!context) {
    throw new Error('useDisplayMode must be used within a DisplayModeProvider');
  }
  return context;
};

export { DisplayModeProvider, useDisplayMode };
