import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import { DisplayModeProvider } from '../src/contexts/DisplayModeContext';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <DisplayModeProvider>
    <App />
    </DisplayModeProvider>
);
