// MyButton.test.js
import React from 'react';
import renderer from 'react-test-renderer';
import MyButton from './MyButton';

test('MyButton snapshot', () => {
  const tree = renderer.create(<MyButton onClick={() => {}}>Click me</MyButton>).toJSON();
  expect(tree).toMatchSnapshot();
});
