import React from 'react'
import classes from './MyModal.module.css'

export default function MyModal({open, children, onClose}) {
  
  const handleClose = () => {
    onClose();
  };

  const handleOverlayClick = (e) => {
    if (e.target.classList.contains(classes.overlay)) {
      onClose();
    }
  };

  if (!open) return null

  return (
    <>
        <div className={classes.overlay}  onClick={handleOverlayClick}></div>
        <div className={classes.myMdl}>
        <button onClick={handleClose} className={classes.closeX}>X</button>
        {children}
        </div>
    </>
  )
}