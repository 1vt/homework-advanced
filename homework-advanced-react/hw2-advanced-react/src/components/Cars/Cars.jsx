import React, { useEffect, useState } from "react";
import { sendRequest } from "../../helpers/sendRequest";
import "./Car.scss";
// import { ReactComponent as Heart } from './icons/Heart.svg';
import MySVGCar from "../MySVGComponent/MySVGCar";
import { useRef } from "react";
import MyButton from "../Button/MyButton";
import MyModal from "../Modal/MyModal";
import ModalHeader from "../Modal/ModalHeader";
import ModalBody from "../Modal/ModalBody";
import ModalWrapper from "../Modal/ModalWrapper";
import ModalFooter from "../Modal/ModalFooter";
import Header from '../../composition/Header/Header'

const Cars = () => {
  const [jsonData, setProducts] = useState([]);

  useEffect(() => {
    sendRequest("/products.json").then((result) => {
      setProducts(result);
    });
  }, []);

  const [isOpen, setIsOpen] = useState(false);
  let menuRef = useRef();

  useEffect(() => {
    let handler = (e) => {
      if (e.target) {
        setIsOpen(false);
      }
    };
    document.addEventListener("mousedown", handler);
  });

  // const [addedCount, setAddedCount] = useState(0);
  // const handleAddToCart = () => {
  //   setAddedCount(addedCount + 1);
  // };

  const [addedCount, setAddedCount] = useState(
    parseInt(localStorage.getItem('addedCount')) || 0
  );

  const handleAddToCart = () => {
    setAddedCount(addedCount + 1);
  };

  const handleRemoveFromCart = () => {
    setAddedCount(Math.max(0, addedCount - 1));
  };

  const [addedCars, setAddedCars] = useState({});

  // const [addedBasket, setAddedBasket] = useState(0);

  const [addedBasket, setAddedBasket] = useState(

    parseInt(localStorage.getItem('addedBasket')) || 0
  );

  const handleAddToBasket = (carId) => {

    if (!addedCars[carId]) {
      setAddedBasket(addedBasket + 1);
    }
  };

  return (
    <div className="allCars">
    <Header addedCount={addedCount} addedBasket={addedBasket}/>
      <ul className="carList">
        {jsonData.map((item, index) => (
          <li key={index} className="carItem">
            <img src={item.imagePath} alt={item.name} className="carImg" />
            <div className="carItemDiv">
              {" "}
              <p className="carName">
                <strong>Name:</strong> {item.name}
              </p>
              <p className="carPrice">
                <strong>Price:</strong> ${item.price}
              </p>
              <p className="article">
                <strong>Article:</strong> {item.article}
              </p>
              <p className="color">
                <strong>Color:</strong> {item.color}
              </p>
              <MySVGCar  onAddToCart={handleAddToCart} onRemoveFromCart={handleRemoveFromCart}
              isInCart={addedCount > 0}/>
            </div>
            <div>
              
              <MyButton onClick={() => setIsOpen(true)} onAddToBasket={handleAddToBasket}>Buy this car</MyButton>
            </div>
            <MyModal
              open={isOpen}
              onClose={() => setIsOpen(false)}
              ref={menuRef}
            >
              <ModalWrapper></ModalWrapper>
              <ModalHeader>Nice Choice!</ModalHeader>
              <ModalBody>
                By clicking the "Yes, Add" button, car will be add to buy.
              </ModalBody>
              <ModalFooter  onClose={() => setIsOpen(false)}></ModalFooter>
            </MyModal>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Cars;
