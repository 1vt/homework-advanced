import React, { useState } from "react";
import Header from "../../composition/Header/Header";

const MySVGCar = ({onAddToCart,onRemoveFromCart, isInCart }) => {
  const [isClicked, setIsClicked] = useState(isInCart);

  const handleClick = () => {
    setIsClicked(!isClicked);
    if (!isClicked) {
      onAddToCart(); // Додати до кошика
    } else {
      onRemoveFromCart(); // Видалити з кошика
    }
  };

  const svgStyle = {
    fill: isClicked ? "red" : "black",

  };

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="80px"
      height="80px"
      viewBox="10 10 54 54"
      onClick={handleClick}
      style={svgStyle}
    >
      <g>
        <path d="M21.9949 16.9301C20.4953 15.1826 17.9948 
          14.7125 16.116 16.3127C14.2372 17.9129 13.9727 20.5884 15.4481
          22.481C16.6749 24.0545 20.3873 27.3732 21.6041 28.4474C21.7402
          28.5675 21.8083 28.6276 21.8877 28.6512C21.957 28.6718 22.0328
          28.6718 22.1021 28.6512C22.1815 28.6276 22.2495 28.5675 22.3857
          28.4474C23.6024 27.3732 27.3149 24.0545 28.5416 22.481C30.017
          20.5884 29.7848 17.8961 27.8737 16.3127C25.9626 14.7294 23.4944 15.1826 21.9949 16.9301Z" />
      </g>
    </svg>
  );
};
export default MySVGCar;
