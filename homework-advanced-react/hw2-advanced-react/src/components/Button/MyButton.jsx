import React from "react";
import classes from './MyButton.module.css'

const MyButton = ({children, onClick, onAddToBasket}) => {
  return (
    <button onClick={(e) => { onClick(); onAddToBasket(); }} className={classes.myBtn}>
        {children}
    </button>
  );
};

export default MyButton;
