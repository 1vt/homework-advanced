import React, {useState} from "react";
import classes from "./ModalFooter.module.css";


export default function ModalFooter({open, onAddToBasket, onClose }) {

  const handleYesClick = () => {
    onAddToBasket();
    onClose() // Викликаємо функцію з компонента Cars
  };

  const handleNoClick  = () => {
    onClose(); // Закриваємо модальне вікно при кліку на "NO, CANCEL"
  };

  return (
    <div className={classes.firstModalBtn}>
      <button className={classes.cancelBtn} onClick={handleYesClick}>
        YES, ADD</button>
      <button className={classes.yesBtn} onClick={handleNoClick }>
        NO, CANCEL</button>
    </div>


  );
}