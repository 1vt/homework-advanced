import React, {useState} from "react";
import Header from "./composition/Header/Header";
import './styles/reset.scss';
import './styles/App.scss';
import Footer from "./composition/Footer/Footer";
import Cars from "./components/Cars/Cars";



function App() {
  const state = useState(3)
  console.log(state);

  return (
    <div className="App">
      {/* <Header/> */}
      <Cars/>
      <Footer/>
    </div>
  );
}

export default App;
