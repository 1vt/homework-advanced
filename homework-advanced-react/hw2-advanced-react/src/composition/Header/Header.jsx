import logoicon from "./icons/carlogo.png"
import "./Header.scss"
import React, {useEffect} from 'react'
import favHeart from './icons/favotire.jpg'
import basket from './icons/basket.png'
import MySVGCar from "../../components/MySVGComponent/MySVGCar"

const Header = ({addedCount, addedBasket}) =>{

  useEffect(() => {
    localStorage.setItem('addedCount', addedCount);
    localStorage.setItem('addedBasket', addedBasket);
  }, [addedCount, addedBasket]);

    return(
        <header className="header">
              <a href="#"><img src={logoicon} alt="logo" className="logo"/></a>
              <nav className="nav">
                <p className="nav-text">NEW</p>
                <p className="nav-text">TYPES</p>
                <p className="nav-text">BRANDS</p>
                <p className="nav-text">EXCLUSIVE</p>
                <p className="nav-text">SERVICE</p>
              <aside className="headerAside"> 
                <div><p className="favCount">{addedCount}</p><img src={favHeart} alt="heart" className="fav"/></div>
                <form><input placeholder="search" className="nav-input"></input></form>
                <div><p className="basCount">{addedBasket}</p><img src={basket} alt="basket" className="basket"/></div>
              </aside>
              </nav>
        </header>
    )
}
export default Header