import React, { useEffect, useState } from "react";
import { sendRequest } from "../../helpers/sendRequest";
import "./Car.scss";
import MySVGCar from "../../components/MySVGComponent/MySVGCar";
import { useRef } from "react";
import MyButton from "../../components/Button/MyButton";
import MyModal from "../../components/Modal/MyModal";
import ModalHeader from "../../components/Modal/ModalHeader";
import ModalBody from "../../components/Modal/ModalBody";
import ModalWrapper from "../../components/Modal/ModalWrapper";
import ModalFooter from "../../components/Modal/ModalFooter";
import Header from '../../composition/Header/Header'
import products from "../../data/products.json";

const Cars = ({ favorites, setFavorites, basket, setBasket }) => {
  const [jsonData, setProducts] = useState([]);

  useEffect(() => {
    setProducts(products);
  }, []);

  const [isOpen, setIsOpen] = useState(false);
  let menuRef = useRef();

  useEffect(() => {
    let handler = (e) => {
      if (e.target) {
        setIsOpen(false);
      }
    };
    document.addEventListener("mousedown", handler);
  }, []);

  const [addedCount, setAddedCount] = useState(
    parseInt(localStorage.getItem("addedCount")) || 0
  );

  const handleRemoveFromCart = () => {
    setAddedCount(Math.max(0, addedCount - 1));
  };

  const [addedBasket, setAddedBasket] = useState(
    parseInt(localStorage.getItem("addedBasket")) || 0
  );

  const handleAddToFavorites = (car) => {
    console.log('Adding to favorites:', car);
    setFavorites([...favorites, car]);
  };

  const handleModalClose = () => {
    setIsOpen(false);
  };

  useEffect(() => {
    console.log("Products loaded:", products);
    setProducts(products);
  }, []);  


  const handleAddToCart = (car) => {
    console.log("Adding to cart:", car);
    setAddedCount(addedCount + 1);
    setBasket([...basket, car]);
  };
  
  const handleAddToBasket = (car) => {
    console.log("Adding to basket:", car);
    setAddedBasket(addedBasket + 1);
    setBasket([...basket, car]);
    localStorage.setItem("addedBasket", addedBasket + 1);
    localStorage.setItem("basket", JSON.stringify([...basket, car]));
  };
  
  const openModal = (car) => {
    handleAddToBasket(car);
    setIsOpen(true);
  };

  return (
    <div className="allCars">
      <Header addedCount={addedCount} addedBasket={addedBasket} />
      <ul className="carList">
        {jsonData.map((item, index) => (
          <li key={index} className="carItem">
            <img src={item.imagePath} alt={item.name} className="carImg" />
            <div className="carItemDiv">
              <p className="carName">
                <strong>Name:</strong> {item.name}
              </p>
              <p className="carPrice">
                <strong>Price:</strong> ${item.price}
              </p>
              <p className="article">
                <strong>Article:</strong> {item.article}
              </p>
              <p className="color">
                <strong>Color:</strong> {item.color}
              </p>
              <MySVGCar
                onAddToCart={() => handleAddToCart(item)}
                onRemoveFromCart={handleRemoveFromCart}
                isInCart={addedCount > 0}
                onAddToFavorites={() => handleAddToFavorites(item)}
                onClick={() => handleAddToFavorites(item)}
              />
            </div>
            <div>
              <MyButton
                onClick={() => openModal(item)}
                onAddToBasket={() => handleAddToBasket(item)}
                
              >
                Buy this car
              </MyButton>
            </div>
            <MyModal open={isOpen} onClose={handleModalClose} ref={menuRef}>
              <ModalWrapper></ModalWrapper>
              <ModalHeader>Nice Choice!</ModalHeader>
              <ModalBody>
                By clicking the "Yes, Add" button, car will be added to the basket.
              </ModalBody>
              <ModalFooter onClose={handleModalClose}></ModalFooter>
            </MyModal>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Cars;


