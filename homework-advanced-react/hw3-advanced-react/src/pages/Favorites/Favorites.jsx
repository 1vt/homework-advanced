import React from "react";
import Header from "../../composition/Header/Header";
import "./Favorites.scss";

const Favorites = ({ favorites, removeFromFavorites  }) => {

  return (
    <>
      <Header />
      <div className="favorites">
        <h2>Favorites</h2>
        {favorites && favorites.length > 0 ? (
          <ul>
            {favorites.map((item) => (
              <li key={item.id}>
                <img src={item.imagePath} alt={item.name} className="img" />
                {item.name} - {item.price}
                <button onClick={() => removeFromFavorites(item.id)}>Remove from Favorites</button>
              </li>
            ))}
          </ul>
        ) : (
          <p>No favorites yet.</p>
        )}
      </div>
    </>
  );
};

export default Favorites;
