// 1) Є два елементи і якщо елемент 1 є прототипом 2 то він буде успадковувати його властиввості.
// 2) Для того щоб виконати його батьківський конструктор.

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }

  set name(newName) {
    this._name = newName;
  }

  get age() {
    return this._age;
  }

  set age(newAge) {
    this._age = newAge;
  }

  get salary() {
    return this._salary;
  }

  set salary(newSalary) {
    this._salary = newSalary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  get salary() {
    return super.salary * 3;
  }

  get programmingLanguages() {
    return this.lang;
  }
  set programmingLanguages(newLang) {
    this.lang = newLang;
  }
}

const programmer1 = new Programmer("Mickey Mouse", 21, 50000, ["JavaScript", "Python", "Java"]);
const programmer2 = new Programmer("Donald Duck", 28, 55000, ["C#", "C++", "Swift"]);
const programmer3 = new Programmer("Goofy", 32, 60000, ["Ruby", "PHP", "SQL"]);

console.log("Disneyland:");
console.log(`Developer 1: Name - ${programmer1.name}, Age - ${programmer1.age}, Salary - ${programmer1.salary}, Programming Languages - ${programmer1.programmingLanguages}`);
console.log(`Developer 2: Name - ${programmer2.name}, Age - ${programmer2.age}, Salary - ${programmer2.salary}, Programming Languages - ${programmer2.programmingLanguages}`);
console.log(`Developer 3: Name - ${programmer3.name}, Age - ${programmer3.age}, Salary - ${programmer3.salary}, Programming Languages - ${programmer3.programmingLanguages}`);