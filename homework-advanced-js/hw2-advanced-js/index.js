/* 
1) Коли ми не впевнені у ідельній роботі функції то ми можемо використати try-catch
*/

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

function isValidBook(book) {
  const allProp = ['author', 'name', 'price'];
  for (const prop of allProp) {
    if (!book.hasOwnProperty(prop)) {
      console.error(`Об'єкт не має властивості: ${prop}`);
      return false;
    }
  }
  return true;
}
function createList(books) {
  const div = document.createElement('div');
  div.id = 'root'
  document.body.append(div)
  const ul = document.createElement('ul');

  books.forEach((book) => {
    try {
      if (isValidBook(book)) {
        const li = document.createElement('li');
        li.textContent = "Автор: " + book.author + ", Назва: " + book.name + ", Ціна: " + book.price + " грн";
        ul.appendChild(li);
      } else {
        throw new Error('Об\'єкт не містить всі необхідні властивості');
      }
    } catch (error) {
      console.error(error.message);
    }
  });
  div.appendChild(ul);
}
createList(books);