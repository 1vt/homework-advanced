// Можливість виконувати функції JS з затримкою тобто асинхронно.

const btn = document.createElement('button')
btn.style.width='100px'
btn.style.height='100px'
btn.innerText=`Do NOT press!`
btn.style.background='red'
document.body.append(btn)
btn.addEventListener('mousedown', ()=>{
    btn.style.background='white'
})
btn.addEventListener('mouseup', ()=>{
    btn.style.background='red'
})

document.addEventListener('DOMContentLoaded', () => {
    const ipInfoDiv = document.getElementById('ipInfo');
    btn.addEventListener('click', async () => {
        try {
            const ipResponse = await fetch('https://api.ipify.org/?format=json');
            const ipData = await ipResponse.json();

            const ipAddress = ipData.ip;
            ipInfoDiv.innerHTML = `IP адреса клієнта: ${ipAddress}<br>`;

            const locationResponse = await fetch(`https://ip-api.com/json/${ipAddress}`);
            const locationData = await locationResponse.json();

            const continent = locationData.continent;
            const country = locationData.country;
            const region = locationData.regionName;
            const city = locationData.city;
            const district = locationData.district;

            ipInfoDiv.innerHTML += `
                Континент: ${continent}<br>
                Країна: ${country}<br>
                Регіон: ${region}<br>
                Місто: ${city}<br>
                Район: ${district}
            `;
            console.log(`Континент: ${continent} Країна: ${country}    Регіон: ${region}      Місто: ${city}    Район: ${district}`);   } catch (error) {
          ipInfoDiv.innerHTML = 'Помилка при отриманні інформації про IP адресу та фізичну адресу.';
            console.error(error);
        }
    });
});
