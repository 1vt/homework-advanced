// Щоб у фоновому режимі надсилати запити на сервер і довантажувати потрібні користувачу дані.

const API = "https://ajax.test-danit.com/api/swapi/films";
const all = document.createElement("div");

const sendRequest = (url, method = "GET", options) => {
  return fetch(url, { method: method, ...options }).then((response) =>
    response.json()
  );
};

function getFilmsList(element) {
  element.classList.add("all");
  document.body.prepend(element);
  sendRequest(API)
    .then((data) => {
      return Promise.all(data.map((episode) => {
        const { episodeId, name, openingCrawl, characters } = episode;
        const films = { name, characters: [], episodeId, openingCrawl };
        console.log("films", films);
        
        const characterPromises = characters.map((people) =>
          sendRequest(people).then((data) => data.name)
        );

        return Promise.all(characterPromises).then((characterData) => ({
          episodeId,
          name,
          openingCrawl,
          characters: characterData.join(", "),
        }));
      }));
    })
    .then((filmDataArray) => {
      filmDataArray.forEach((filmData) => {
        element.insertAdjacentHTML(
          "afterbegin",
          `
            <ul class="all__list list">    
              <li class="list__episode">Episode: ${filmData.episodeId}</li>
              <li class="list__name">Name: ${filmData.name}</li>
              <li class="list__characters">Characters: ${filmData.characters}</li>
              <li class="list__topic">Summary: ${filmData.openingCrawl}</li>
            </ul>
          `
        );
      });
    });
}
getFilmsList(all);
