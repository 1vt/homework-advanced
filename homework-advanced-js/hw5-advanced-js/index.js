const nameList = document.createElement('h');
nameList.innerText = "People List"
document.body.append(nameList)

const postsContainer = document.createElement('div')
postsContainer.classList.add = ("posts-container")
document.body.append(postsContainer)

class Card {
    constructor(postData) {
        this.postData = postData;
        this.cardElement = this.createCardElement();
    }
    
    createCardElement() {
        const card = document.createElement("div");
        card.classList.add("card");

        const header = document.createElement("h2");
        header.textContent = this.postData.title;

        const text = document.createElement("p");
        text.textContent = this.postData.body;

        const user = document.createElement("p");
        user.textContent = `${this.postData.user.name} (${this.postData.user.email})`;

        const deleteButton = document.createElement("button");
        deleteButton.textContent = "Видалити";
        deleteButton.classList.add("delete-button");
        deleteButton.addEventListener("click", () => this.deletePost());

        card.appendChild(header);
        card.appendChild(text);
        card.appendChild(user);
        card.appendChild(deleteButton);

        return card;
    }

    deletePost() {
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.postData.id}`, {
            method: "DELETE"
        })
        .then(response => {
            if (response.status === 200) {
                this.cardElement.remove();
            } else {
                alert("Помилка при видаленні публікації");
            }
        })}
}

function fetchAndDisplayPosts() {
    fetch("https://ajax.test-danit.com/api/json/users")
        .then(response => response.json())
        .then(users => {
            fetch("https://ajax.test-danit.com/api/json/posts")
                .then(response => response.json())
                .then(posts => {
                    const allPosts = posts.slice(0, 10);
                    allPosts.forEach(post => {
                        const user = users.find(user => user.id === post.userId);
                        if (user) {
                            post.user = user;
                            const card = new Card(post);
                            postsContainer.appendChild(card.cardElement);
                        }
                    });
                })
                .catch(error => {
                    console.error(error);
                    alert("Помилка при отриманні публікацій");
                })})
}
fetchAndDisplayPosts();